{{- define "subtemplate_timeoffapp.tags" }}
  labels:
    owner: {{ .Values.tags.owner | quote }}
    tribu: {{ .Values.tags.tribu | quote }}
    date: {{ now | htmlDate }}
{{- end }}
